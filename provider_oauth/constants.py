from datetime import timedelta
from django.conf import settings

CLIENT_NAME = getattr(settings, 'CLIENT_NAME', 'teachbase')
# RETURN_URI = getattr(settings, 'RETURN_URI', 'http://med.propeller.su:8000/oauth2/auth/success/')
RETURN_URI = getattr(settings, 'RETURN_URI', 'http://xn--90aialyd0b6a.xn--90acesaqsbbbreoa5e3dp.xn--p1ai/oauth2/auth/success/')

EXPIRE_CODE_DELTA = getattr(settings, 'OAUTH_EXPIRE_CODE_DELTA', timedelta(seconds=10 * 60))
EXPIRE_CODE_USER = getattr(settings, 'OAUTH_EXPIRE_CODE_DELTA', timedelta(minutes=60))

TOKEN_TYPE = 'Bearer'