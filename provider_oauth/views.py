import json

from django.contrib.sites.models import Site
from django.http.response import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView, RedirectView, View
from tldextract import tldextract
from provider_oauth.middleware import ViewException
from .utils import now
from django.conf import settings
from .models import AccessTokenModel, Client
from .constants import *
from provider_oauth.utils import get_value, get_user_expiry
from django.utils.dateformat import format as form_date


class AccessTokenBackend(object):

    def authenticate(self, request):
        access_token = get_value(request, 'access_token')
        if not access_token:
            # return HttpResponseBadRequest(json.dumps({'code': int(1), 'text': 'Not specified access_token'}))
            raise ViewException('json', 'Not specified access_token', 0)
        try:
            token = AccessTokenModel.objects.get(token=access_token)
            if token.expires < now():
                raise ViewException('json', 'Expired token life', 3)
            return token
        except AccessTokenModel.DoesNotExist:
            raise ViewException('json', 'Invalid access token', 2)


class APITeachbase(View):
    http_method_names = ['get']
    method = None
    backend = AccessTokenBackend
    token = None

    def dispatch(self, request, *args, **kwargs):
        self.token = self.backend().authenticate(request)
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, self.method, self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

    def get_user(self, request):
        user = self.token.user
        return JsonResponse({'name': user.first_name, 'last_name': user.last_name, 'email': user.email})


class OAuth(object):
    def __init__(self):
        self.client_id = ''

    def authenticate(self, request=None):
        auth = get_value(request, 'client_api_key')
        if not auth:
            raise Warning('Not specified client_api_key')
        try:
            client = Client.objects.get(client_id=auth)
            # if client.expire < now():
            #     raise OAuthError("Time expired token life")
        except Client.DoesNotExist:
            raise Warning("Client could not be validated with key pair.")
        return client


class SuccessAuthorisation(RedirectView):

    def confirm(self):
        return


@method_decorator(csrf_exempt, name='dispatch')
class AccessToken(RedirectView):
    http_method_names = ['get']
    grant_types = ['client_credentials', 'simplify']
    auth = OAuth()

    def dispatch(self, request, *args, **kwargs):
        response = super(AccessToken, self).dispatch(request, *args, **kwargs)
        response['Cache-Control'] = 'no-store'
        response['Pragma'] = 'no-cache'
        return response

    def authenticate(self, request):
        client = self.auth.authenticate(request)
        if client is not None:
            return client
        return None

    def get(self, request, *args, **kwargs):
        client = self.authenticate(request)
        try:
            if client.get('error', False):
                return self.error_response({'error': client['error'][0]})
        except:
            if client is None:
                return self.error_response({'error': 'Invalid client'})
        if not request.user.is_active:
            return HttpResponseRedirect('/')
        self.request.session['redirect_path'] = request.session.get('redirect_path', '/programs/')
        self.request.session.modified = True
        self.request.session.save()
        token = self.get_access_token(request, client)
        return self.access_token_response(token)

    def get_access_token(self, request, client):
        try:
            user = request.user
            # at = AccessTokenModel.objects.get(user_id=request.user.id, client_id=client.id)
            at = AccessTokenModel.objects.get(user_id=request.user.id, client_id=client.id, expires__gt=now())
        except AccessTokenModel.DoesNotExist:
            at = AccessTokenModel.objects.create(user_id=request.user.id, client_id=client.id, expires=get_user_expiry())
        return at

    def error_response(self, error, content_type='application/json', status=400,
            **kwargs):
        return HttpResponse(json.dumps(error), content_type=content_type,
                status=status, **kwargs)

    def check_url(self, url, client):
        real_url = '%s.%s' % (tldextract.extract(client.url).domain, tldextract.extract(client.url).suffix)
        o = tldextract.extract(url)
        if real_url != '%s.%s' % (o.domain, o.suffix):
            raise Warning('Invalid return uri. Check the main domain.')
        return True

    def access_token_response(self, access_token):
        access_token.client.generate_secret()
        r_url = self.request.GET.get('redirect_url', False)
        if not r_url:
            raise Warning('Not specified redirect_url')
        # self.check_url(r_url, access_token.client)
        site = Site.objects.get_current()
        return HttpResponseRedirect(r_url + '?access_token=%s&uid=%s&email=%s&expires=%s&redirect_url=%s' %(access_token.token, access_token.user.id,
                                                                                                   access_token.user.email.lower(), form_date(access_token.expires, 'U'), 'http://%s/response/course/uri/' % site.domain))


def response_course_uri(request):
    # if request.user.redirect_path:
        # from contents.models import Course
        # course = Course.objects.get(id=request.session['course'])
        # return HttpResponseRedirect(request.user.redirect_path)
    return HttpResponseRedirect('/')
