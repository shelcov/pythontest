import hashlib
import shortuuid
from datetime import datetime

from django import http
from django.conf import settings
from django.utils import dateparse
from django.db.models.fields import (DateTimeField, DateField,
                                     TimeField,
                                     FieldDoesNotExist)
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.encoding import force_text
from django.utils.timezone import now as new
from django.db import connection

from .constants import EXPIRE_CODE_DELTA, EXPIRE_CODE_USER

try:
    import json
except:
    import simplejson as json

try:
    from django.utils import timezone
except ImportError:
    timezone = None


def now():
    if timezone:
        return new()
    else:
        return datetime.now()


def short_token():
    hash = hashlib.sha1(shortuuid.uuid().encode('utf-8'))
    hash.update(settings.SECRET_KEY.encode('utf-8'))
    return hash.hexdigest()[::2]


def long_token():
    hash = hashlib.sha1(shortuuid.uuid().encode('utf-8'))
    hash.update(settings.SECRET_KEY.encode('utf-8'))
    return hash.hexdigest()


def get_token_expiry():
    return now() + EXPIRE_CODE_DELTA


def get_user_expiry():
    return now() + EXPIRE_CODE_USER


def get_body(request, field):
    try:
        return json.loads(force_text(request.body)).get(field)
    except:
        return False


def get_value(request, name):
    return request.GET.get(name) or request.POST.get(name) or get_body(request, name)


def serialize_instance(instance):
    ret = dict([(k, v)
                for k, v in instance.__dict__.items()
                if not k.startswith('_')])
    return json.loads(json.dumps(ret, cls=DjangoJSONEncoder))


def deserialize_instance(model, data={}):
    ret = model()
    for k, v in data.items():
        if v is not None:
            try:
                f = model._meta.get_field(k)
                if isinstance(f, DateTimeField):
                    v = dateparse.parse_datetime(v)
                elif isinstance(f, TimeField):
                    v = dateparse.parse_time(v)
                elif isinstance(f, DateField):
                    v = dateparse.parse_date(v)
            except FieldDoesNotExist:
                pass
        setattr(ret, k, v)
    return ret


class GEOS_JSONEncoder(DjangoJSONEncoder):
    def default(self, o):
        try:
            return o.json  # Will therefore support all the GEOS objects
        except:
            pass
        return super(GEOS_JSONEncoder, self).default(o)


def output_json(out, code=200):
    if code != 200:
        out['code'] = code
    indent = None
    if settings.DEBUG:
        if isinstance(out, dict):
            out['debug_db_queries'] = connection.queries
        indent = 4
    encoder = GEOS_JSONEncoder(ensure_ascii=False, indent=indent)
    content = encoder.iterencode(out)

    # We don't want a generator function (iterencode) to be passed to an
    # HttpResponse, as it won't cache due to its close() function adding it to
    # an instance attribute.
    content = map(lambda x: x, content)

    types = {
        400: http.HttpResponseBadRequest,
        404: http.HttpResponseNotFound,
        500: http.HttpResponseServerError,
    }
    response_type = types.get(code, http.StreamingHttpResponse)

    response = response_type(content_type='application/json; charset=utf-8')
    response['Access-Control-Allow-Origin'] = '*'
    response['Cache-Control'] = 'max-age=2419200'  # 4 weeks
    attr = 'streaming_content' if getattr(response, 'streaming', None) else 'content'
    setattr(response, attr, content)
    return response


def build_content_type(format, encoding='utf-8'):
    if 'charset' in format:
        return format
    if format in ('application/json', 'text/javascript'):
        return format
    return "%s; charset=%s" % (format, encoding)