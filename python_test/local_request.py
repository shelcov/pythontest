# vim:fileencoding=utf-8
import threading

from django.utils.deprecation import MiddlewareMixin

_local_storage = threading.local()


class CurrentRequestMiddleware(MiddlewareMixin):
    def process_request(self, request):
        _local_storage.request = request


def get_current_request():
    """Вызов request из любого места"""
    return getattr(_local_storage, "request", None)