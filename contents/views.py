# vim:fileencoding=utf-8
from django.views.generic import TemplateView


class FrontView(TemplateView):
    template_name = 'front.html'

    def get_context_data(self, **kwargs):
        ctx = {}
        return ctx
