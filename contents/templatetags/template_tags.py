# vim:fileencoding=utf-8
from datetime import datetime, timedelta
import math
import random
from django import template
from django.template import Template, Context
import re
import time
from django.template.loader import render_to_string

from python_test.settings import STATIC_URL

register = template.Library()


def render_field(val, arg):
    t = Template(val)
    c = Context({'item': arg})
    return t.render(c)


def stringToID(val):
    return int(val.split('-')[1].split(' ')[0])


def minus(value, arg):
    return value - int(arg)


def plus(value, arg):
    return int(value) + int(arg)


def multiply(value, arg):
    return int(value) * int(arg)


def split(values, arg):
    if values:
        n = values.split(arg)
        return n
    return False


def replace_unicode(value):
    import re
    r = re.sub(r'&([A-Za-z]+);', '', value)
    return r


def parse_int(value):
    return int(value)


def get_date(value):
    return datetime.strptime(value, '%a, %d %b %Y %H:%M:%S %Z')


def files(values):
    n = int(math.floor(values / 2.00))
    return n


def parse_str(value):
    return str(value)


def price_format(value):
    decimal_points = 3
    separator = u' '
    value = str(value)
    if len(value) <= decimal_points:
        return value
    parts = []
    while value:
        parts.append(value[-decimal_points:])
        value = value[:-decimal_points]
    parts.reverse()
    return separator.join(parts)


def to_class_name(value):
    return value.__class__.__name__


def get_timer(d_from, d_to):
    from datetime import datetime
    now = datetime.now()
    e = time.mktime(d_to.timetuple())-time.mktime(now.timetuple())
    return int(e)


def sizes(value, arg):
    e = random.randint(int(arg.split(',')[0]), int(arg.split(',')[1]))
    return e


def get_million(value):
    return int((int(value)/1000000))


def parse_float(value):
    e, f = str(value).split('.')
    if int(f) > 0:
        return value
    return int(e)


def get_extension(value):
    f = str(value).split('.')
    return f[-1]


def get_arr_el(arr, id):
    if id+1 > len(arr):
        return arr[-1]
    return arr[id]


def is_entry(value, arg):
    if re.findall(arg, value) != 0:
        return True
    return False


def replace(value, param):
    f, e = param.split('#')
    try:
        value = str(value).replace(f, e)
    except:
        value = value.replace(f, e)
    return value


def die_ie(value):
    arr = ['MSIE 8', 'MSIE 7', 'MSIE 6']
    if len([i for i in arr if i in value]) > 0:
        return False
    return True


def compare_date(date):
    from django.utils import timezone
    return timezone.now() < date


def compare_date_without(date):
    from django.utils import timezone
    return timezone.now()-timedelta(days=1) < date


def compare_date_with_min(date, minute):
    from django.utils import timezone
    return timezone.now() + timedelta(minutes=minute) >= date


def isinstance_t(value):
    return isinstance(value, dict)


def ntt(value):
    days = [u'Первый', u'Второй', u'Третий', u'Четвёртый', u'Пятый', u'Шестой', u'Седьмой', u'Восьмой', u'Девятый',
            u'Десятый', u'Одиннадцатый', u'Двенадцатый', u'Тринадцатый']
    try:
        return days[value-1]
    except:
        return value


def sec_to_time(sec):
    import time
    return time.strftime('%H:%M:%S', time.gmtime(int(float(sec))))


def to_dollar(price, user):
    try:
        rate = user.get_rate()
        return int(math.ceil(float(price) * float(rate)))
    except:
        return price


def delete_tags(value):
    tags = re.findall('{\$ \w+-\d+ \$}', value)
    for res in tags:
        value = value.replace(res, '')
    return value


def end_date(value):
    if value:
        from django.utils import timezone
        now = timezone.now()
        if now > value:
            return False
    return True


def digit_module(value):
    return abs(value)


def str_len(value):
    return len(value) * 10


def get_range(value):
    return range(value)


# register.filter('render_tags', render_tags)
register.filter('str_len', str_len)
register.filter('digit_module', digit_module)
register.filter('end_date', end_date)
register.filter('to_dollar', to_dollar)
register.filter('sec_to_time', sec_to_time)
register.filter('ntt', ntt)
register.filter('compare_date_with_min', compare_date_with_min)
register.filter('compare_date_without', compare_date_without)
register.filter('isinstance_t', isinstance_t)
register.filter('render_field', render_field)
register.filter('compare_date', compare_date)
register.filter('die_ie', die_ie)
register.filter('replace', replace)
register.filter('is_entry', is_entry)
register.filter('get_extension', get_extension)
register.filter('get_arr_el', get_arr_el)
register.filter('parse_float', parse_float)
register.filter('get_million', get_million)
register.filter('sizes', sizes)
register.filter('get_timer', get_timer)
register.filter('to_class_name', to_class_name)
register.filter('price', price_format)
register.filter('files', files)
register.filter('parse_date', get_date)
register.filter('str', parse_str)
register.filter('parse_int', parse_int)
register.filter('replace_unicode', replace_unicode)
register.filter('split', split)
register.filter('multiply', multiply)
register.filter('plus', plus)
register.filter('minus', minus)
