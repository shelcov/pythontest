# vim:fileencoding=utf-8
import json
import os
from base64 import b64decode, b64encode
from uuid import uuid4
from datetime import datetime
import requests
from django.core.files.base import ContentFile
import time
from python_test.local_request import get_current_request
from python_test.settings import MEDIA_ROOT, TEACHBASE_DOMAIN, APP_ID, SECRET
from python_test.utils import create_slug


class TApi(object):
    """Класс для работы с Api teachbase"""
    def __init__(self, token=None):
        self.APP_ID = APP_ID
        self.SECRET = SECRET
        self.host = TEACHBASE_DOMAIN
        self.v = '/endpoint/v1/'
        self.headers = {'Content-Type': 'application/json'}
        self.request = get_current_request()
        self.token = token

    def authorise(self):
        # proxies = {
        #     'http': 'http://66.82.123.234:8080',
        #     'https': 'http://89.236.17.106:3128',
        # }
        """Получение токена со сроком действия"""

        response = requests.post('https://go.teachbase.ru/oauth/token',
                                 {'client_id': self.APP_ID, 'client_secret': self.SECRET,
                                  'grant_type': 'client_credentials'}, verify=False)
        body = json.loads(str(response.content, 'utf-8'))
        if body.get('error', False):
            raise ValueError(body.get('error_description'))
        if body.get('access_token', False):
            if not os.path.exists(os.path.join(MEDIA_ROOT)):
                os.mkdir(os.path.join(MEDIA_ROOT))
            doc = open(os.path.join(MEDIA_ROOT, 'ksm3c4er45856.txt'), 'w+')
            code = body['access_token']
            doc.write(b64encode(('{"t": "%s", "e": "%s"}' % (code, body['created_at'] + body['expires_in'])).encode()).decode("utf-8"))
            doc.close()
            self.token = code
            return self.token

    def get_access(self):
        """Проверяется наличие и актуальность токена"""
        if not self.token:
            if not os.path.exists(os.path.join(MEDIA_ROOT, 'ksm3c4er45856.txt')):
                return self.authorise()
            doc = open(os.path.join(MEDIA_ROOT, 'ksm3c4er45856.txt'), 'r+')
            text = doc.read()
            doc.close()
            if len(text):
                d = json.loads(b64decode(text).decode())
                if d.get('t', False) and datetime.now() < datetime.fromtimestamp(int(d['e'])):
                    self.token = d['t']
                else:
                    self.token = self.authorise()
            else:
                self.token = self.authorise()
        return self.token
